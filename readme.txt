Country
    id country
    name
    markup (null)
    prepayment (null)

City
    id city
    id country
    name
    markup (null)
    prepayment (null)

Real Estatement Type
    id
    name

Rent Object Type
    id
    name

Order Status
    id
    name
    number
    
-- -- -- -- 
Users
    id
    email
    password
    fullname

User Roles
    id
    name

RealEstateObject
    id of this
    type id
    city id
    address
    images
    properties

RentObject 
    this.id
    RealEstateObject.id
    AdditionalAddress
    images
    properties
    
