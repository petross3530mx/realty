<?php

namespace App\Realty\Form;

use App\Realty\Entity\RentObject;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RentObjectType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('additionalAddress')
            ->add('images')
            ->add('properties')
            ->add('RealEstateObjectId')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => RentObject::class,
        ]);
    }
}
