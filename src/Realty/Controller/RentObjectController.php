<?php

namespace App\Realty\Controller;

use App\Realty\Entity\RentObject;
use App\Realty\Form\RentObjectType;
use App\Realty\Repository\RentObjectRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/rent/object")
 */
class RentObjectController extends AbstractController
{
    /**
     * @Route("/", name="rent_object_index", methods={"GET"})
     */
    public function index(RentObjectRepository $rentObjectRepository): Response
    {
        return $this->render('rent_object/index.html.twig', [
            'rent_objects' => $rentObjectRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="rent_object_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $rentObject = new RentObject();
        $form = $this->createForm(RentObjectType::class, $rentObject);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($rentObject);
            $entityManager->flush();

            return $this->redirectToRoute('rent_object_index');
        }

        return $this->render('rent_object/new.html.twig', [
            'rent_object' => $rentObject,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="rent_object_show", methods={"GET"})
     */
    public function show(RentObject $rentObject): Response
    {
        return $this->render('rent_object/show.html.twig', [
            'rent_object' => $rentObject,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="rent_object_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, RentObject $rentObject): Response
    {
        $form = $this->createForm(RentObjectType::class, $rentObject);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('rent_object_index');
        }

        return $this->render('rent_object/edit.html.twig', [
            'rent_object' => $rentObject,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="rent_object_delete", methods={"DELETE"})
     */
    public function delete(Request $request, RentObject $rentObject): Response
    {
        if ($this->isCsrfTokenValid('delete'.$rentObject->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($rentObject);
            $entityManager->flush();
        }

        return $this->redirectToRoute('rent_object_index');
    }
}
