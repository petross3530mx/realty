<?php

namespace App\Realty\Controller;

use App\Realty\Entity\RealEstatementType;
use App\Realty\Form\RealEstatementTypeType;
use App\Realty\Repository\RealEstatementTypeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/real/estatement/type")
 */
class RealEstatementTypeController extends AbstractController
{
    /**
     * @Route("/", name="real_estatement_type_index", methods={"GET"})
     */
    public function index(RealEstatementTypeRepository $realEstatementTypeRepository): Response
    {
        return $this->render('real_estatement_type/index.html.twig', [
            'real_estatement_types' => $realEstatementTypeRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="real_estatement_type_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $realEstatementType = new RealEstatementType();
        $form = $this->createForm(RealEstatementTypeType::class, $realEstatementType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($realEstatementType);
            $entityManager->flush();

            return $this->redirectToRoute('real_estatement_type_index');
        }

        return $this->render('real_estatement_type/new.html.twig', [
            'real_estatement_type' => $realEstatementType,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="real_estatement_type_show", methods={"GET"})
     */
    public function show(RealEstatementType $realEstatementType): Response
    {
        return $this->render('real_estatement_type/show.html.twig', [
            'real_estatement_type' => $realEstatementType,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="real_estatement_type_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, RealEstatementType $realEstatementType): Response
    {
        $form = $this->createForm(RealEstatementTypeType::class, $realEstatementType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('real_estatement_type_index');
        }

        return $this->render('real_estatement_type/edit.html.twig', [
            'real_estatement_type' => $realEstatementType,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="real_estatement_type_delete", methods={"DELETE"})
     */
    public function delete(Request $request, RealEstatementType $realEstatementType): Response
    {
        if ($this->isCsrfTokenValid('delete'.$realEstatementType->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($realEstatementType);
            $entityManager->flush();
        }

        return $this->redirectToRoute('real_estatement_type_index');
    }
}
