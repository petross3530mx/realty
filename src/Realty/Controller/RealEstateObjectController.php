<?php

namespace App\Realty\Controller;

use App\Realty\Entity\RealEstateObject;
use App\Realty\Form\RealEstateObjectType;
use App\Realty\Repository\RealEstateObjectRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/real/estate/object")
 */
class RealEstateObjectController extends AbstractController
{
    /**
     * @Route("/", name="real_estate_object_index", methods={"GET"})
     */
    public function index(RealEstateObjectRepository $realEstateObjectRepository): Response
    {
        return $this->render('real_estate_object/index.html.twig', [
            'real_estate_objects' => $realEstateObjectRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="real_estate_object_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $realEstateObject = new RealEstateObject();
        $form = $this->createForm(RealEstateObjectType::class, $realEstateObject);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($realEstateObject);
            $entityManager->flush();

            return $this->redirectToRoute('real_estate_object_index');
        }

        return $this->render('real_estate_object/new.html.twig', [
            'real_estate_object' => $realEstateObject,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="real_estate_object_show", methods={"GET"})
     */
    public function show(RealEstateObject $realEstateObject): Response
    {
        return $this->render('real_estate_object/show.html.twig', [
            'real_estate_object' => $realEstateObject,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="real_estate_object_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, RealEstateObject $realEstateObject): Response
    {
        $form = $this->createForm(RealEstateObjectType::class, $realEstateObject);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('real_estate_object_index');
        }

        return $this->render('real_estate_object/edit.html.twig', [
            'real_estate_object' => $realEstateObject,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="real_estate_object_delete", methods={"DELETE"})
     */
    public function delete(Request $request, RealEstateObject $realEstateObject): Response
    {
        if ($this->isCsrfTokenValid('delete'.$realEstateObject->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($realEstateObject);
            $entityManager->flush();
        }

        return $this->redirectToRoute('real_estate_object_index');
    }
}
