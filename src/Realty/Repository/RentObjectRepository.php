<?php

namespace App\Realty\Repository;

use App\Realty\Entity\RentObject;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RentObject|null find($id, $lockMode = null, $lockVersion = null)
 * @method RentObject|null findOneBy(array $criteria, array $orderBy = null)
 * @method RentObject[]    findAll()
 * @method RentObject[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RentObjectRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RentObject::class);
    }

    // /**
    //  * @return RentObject[] Returns an array of RentObject objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RentObject
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
