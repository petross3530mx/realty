<?php

namespace App\Realty\Repository;

use App\Realty\Entity\RealEstateObject;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RealEstateObject|null find($id, $lockMode = null, $lockVersion = null)
 * @method RealEstateObject|null findOneBy(array $criteria, array $orderBy = null)
 * @method RealEstateObject[]    findAll()
 * @method RealEstateObject[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RealEstateObjectRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RealEstateObject::class);
    }

    // /**
    //  * @return RealEstateObject[] Returns an array of RealEstateObject objects
    //  */

    public function findByAddressField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.address like :val')
            ->setParameter('val', "%".$value."%")
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(100)
            ->getQuery()
            ->getResult()
        ;
    }




}
