<?php

namespace App\Realty\Repository;

use App\Realty\Entity\RealEstatementType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RealEstatementType|null find($id, $lockMode = null, $lockVersion = null)
 * @method RealEstatementType|null findOneBy(array $criteria, array $orderBy = null)
 * @method RealEstatementType[]    findAll()
 * @method RealEstatementType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RealEstatementTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RealEstatementType::class);
    }

    // /**
    //  * @return RealEstatementType[] Returns an array of RealEstatementType objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RealEstatementType
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
