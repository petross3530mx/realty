<?php

namespace App\Realty\Repository;

use App\Realty\Entity\RentObjectType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RentObjectType|null find($id, $lockMode = null, $lockVersion = null)
 * @method RentObjectType|null findOneBy(array $criteria, array $orderBy = null)
 * @method RentObjectType[]    findAll()
 * @method RentObjectType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RentObjectTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RentObjectType::class);
    }

    // /**
    //  * @return RentObjectType[] Returns an array of RentObjectType objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RentObjectType
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
