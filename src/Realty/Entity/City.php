<?php

namespace App\Realty\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Realty\Repository\CityRepository")
 */
class City
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;
    /* ManyToOne description: One country can be set on One city object,
    but Many  cities objects can be linked to one country  */
    /**
     * @ORM\ManyToOne(targetEntity="App\Realty\Entity\Country")
     * @ORM\JoinColumn(nullable=false)
     */
    private $country;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=8, nullable=true)
     */
    private $markup;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=8, nullable=true)
     */
    private $prepayment;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCountry(): ?Country
    {
        return $this->country;
    }

    public function setCountry(?Country $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getMarkup(): ?string
    {
        return $this->markup;
    }

    public function setMarkup(?string $markup): self
    {
        $this->markup = $markup;

        return $this;
    }

    public function getPrepayment(): ?string
    {
        return $this->prepayment;
    }

    public function setPrepayment(?string $prepayment): self
    {
        $this->prepayment = $prepayment;

        return $this;
    }
}
