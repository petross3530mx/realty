<?php

namespace App\Realty\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Realty\Repository\RentObjectRepository")
 */
class RentObject
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Realty\Entity\RealEstateObject")
     * @ORM\JoinColumn(nullable=false)
     */
    private $RealEstateObjectId;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $additionalAddress;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $images = [];

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $properties = [];

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRealEstateObjectId(): ?RealEstateObject
    {
        return $this->RealEstateObjectId;
    }

    public function setRealEstateObjectId(?RealEstateObject $RealEstateObjectId): self
    {
        $this->RealEstateObjectId = $RealEstateObjectId;

        return $this;
    }

    public function getAdditionalAddress(): ?string
    {
        return $this->additionalAddress;
    }

    public function setAdditionalAddress(?string $additionalAddress): self
    {
        $this->additionalAddress = $additionalAddress;

        return $this;
    }

    public function getImages(): ?array
    {
        return $this->images;
    }

    public function setImages(?array $images): self
    {
        $this->images = $images;

        return $this;
    }

    public function getProperties(): ?array
    {
        return $this->properties;
    }

    public function setProperties(?array $properties): self
    {
        $this->properties = $properties;

        return $this;
    }
}
