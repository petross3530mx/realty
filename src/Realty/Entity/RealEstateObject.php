<?php

namespace App\Realty\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Realty\Repository\RealEstateObjectRepository")
 */
class RealEstateObject
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /* ManyToOne description: One type can be set for one object,
     but on Many realty objects can be linked to one type */
    /**
     * @ORM\ManyToOne(targetEntity="App\Realty\Entity\RealEstatementType")
     * @ORM\JoinColumn(nullable=false)
     */
    private $typeid;

    /* ManyToOne description: One type can be set on Ony realty object,
    but Many  realty objects can be linked to one city  */
    /**
     * @ORM\ManyToOne(targetEntity="App\Realty\Entity\City")
     * @ORM\JoinColumn(nullable=false)
     */
    private $cityid;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $address;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $images = [];

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $properties = [];

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTypeid(): ?RealEstatementType
    {
        return $this->typeid;
    }

    public function setTypeid(?RealEstatementType $typeid): self
    {
        $this->typeid = $typeid;

        return $this;
    }

    public function getCityid(): ?City
    {
        return $this->cityid;
    }

    public function setCityid(?City $cityid): self
    {
        $this->cityid = $cityid;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getImages(): ?array
    {
        return $this->images;
    }

    public function setImages(?array $images): self
    {
        $this->images = $images;

        return $this;
    }

    public function getProperties(): ?array
    {
        return $this->properties;
    }

    public function setProperties(?array $properties): self
    {
        $this->properties = $properties;

        return $this;
    }
}
