<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200427152352 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, fullname VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE order_status (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, comment VARCHAR(1000) NULL, number INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE rent_object (id INT AUTO_INCREMENT NOT NULL, real_estate_object_id_id INT NOT NULL, additional_address VARCHAR(255) DEFAULT NULL, images LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', properties LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', INDEX IDX_8A4EC3A7358AF313 (real_estate_object_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_roles (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE real_estatement_type (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE real_estate_object (id INT AUTO_INCREMENT NOT NULL, typeid_id INT NOT NULL, cityid_id INT NOT NULL, address VARCHAR(255) NOT NULL, images LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', properties LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', INDEX IDX_4F1DF1FF56E2B44 (typeid_id), INDEX IDX_4F1DF1FC12FD324 (cityid_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE city (id INT AUTO_INCREMENT NOT NULL, country_id INT NOT NULL, name VARCHAR(255) NOT NULL, markup NUMERIC(10, 8) DEFAULT NULL, prepayment NUMERIC(10, 8) DEFAULT NULL, INDEX IDX_2D5B0234F92F3E70 (country_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE rent_object_type (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE country (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, markup NUMERIC(10, 8) DEFAULT NULL, prepayment NUMERIC(10, 8) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE rent_object ADD CONSTRAINT FK_8A4EC3A7358AF313 FOREIGN KEY (real_estate_object_id_id) REFERENCES real_estate_object (id)');
        $this->addSql('ALTER TABLE real_estate_object ADD CONSTRAINT FK_4F1DF1FF56E2B44 FOREIGN KEY (typeid_id) REFERENCES real_estatement_type (id)');
        $this->addSql('ALTER TABLE real_estate_object ADD CONSTRAINT FK_4F1DF1FC12FD324 FOREIGN KEY (cityid_id) REFERENCES city (id)');
        $this->addSql('ALTER TABLE city ADD CONSTRAINT FK_2D5B0234F92F3E70 FOREIGN KEY (country_id) REFERENCES country (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE real_estate_object DROP FOREIGN KEY FK_4F1DF1FF56E2B44');
        $this->addSql('ALTER TABLE rent_object DROP FOREIGN KEY FK_8A4EC3A7358AF313');
        $this->addSql('ALTER TABLE real_estate_object DROP FOREIGN KEY FK_4F1DF1FC12FD324');
        $this->addSql('ALTER TABLE city DROP FOREIGN KEY FK_2D5B0234F92F3E70');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE order_status');
        $this->addSql('DROP TABLE rent_object');
        $this->addSql('DROP TABLE user_roles');
        $this->addSql('DROP TABLE real_estatement_type');
        $this->addSql('DROP TABLE real_estate_object');
        $this->addSql('DROP TABLE city');
        $this->addSql('DROP TABLE rent_object_type');
        $this->addSql('DROP TABLE country');
    }
}
